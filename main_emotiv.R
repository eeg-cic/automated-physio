rm(list=ls())
cat("\014")

script.dir <- dirname(sys.frame(1)$ofile)
setwd(script.dir)

list.of.packages <- c('tidyverse', 'openxlsx')
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)

library('tidyverse')
library('openxlsx')

# filename <- 'AA13EmotivS1_Physio_synchronized.txt'
# raw_data <- read_table2(filename,col_names = c('time','neutre','faible','intense','frisson','song','gsr','hr','hrv'))

compute_mean_emotiv <- function(raw_data) {
  
  song_pair <- which(raw_data$song > 0)
  pause_pair <- song_pair[2:(length(song_pair)-1)]
  
  # for pause
  pause_gsr_vector <- c()
  pause_hr_vector <- c() 
  for(i in seq(1, 14, 2)){
    pause_start <- pause_pair[i]+1
    pause_end <- pause_pair[i+1]-1
    pause_section <- slice(raw_data,pause_start:pause_end)
    pause_section <- filter(pause_section,pause_section$gsr>1)
    pause_section <- filter(pause_section,pause_section$hr>1)
    pause_gsr_vector <- c(pause_gsr_vector,mean(pause_section$gsr))
    pause_hr_vector <- c(pause_hr_vector,mean(pause_section$hr))
  }
  pause_gsr <- mean(pause_gsr_vector)
  pause_hr <- mean(pause_hr_vector)
  
  # for song
  neutre_gsr_vector <- c()
  neutre_hr_vector <- c()
  faible_gsr_vector <- c()
  faible_hr_vector <- c()
  intense_gsr_vector <- c()
  intense_hr_vector <- c()
  frisson_gsr_vector <- c()
  frisson_hr_vector <- c()
  for(j in seq(1, 16, 2)){
    tensec_offset <- floor( (song_pair[j+1] - song_pair[j])/90 ) * 10
    song_start <- song_pair[j]+tensec_offset
    song_end <- song_pair[j+1]-1
    song_section <- slice(raw_data,song_start:song_end)
    song_section <- filter(song_section,song_section$gsr>1)
    song_section <- filter(song_section,song_section$hr>1)
    song_section$emotion <- song_section$neutre + song_section$faible + song_section$intense + song_section$frisson
    song_section <- song_section[,c('emotion','gsr','hr')]
    dummy <- tibble(emotion = 9,gsr=0,hr=0)
    song_section <- bind_rows(dummy,song_section,dummy)
    emotion_marker <- which(c(FALSE, tail(song_section$emotion,-1) != head(song_section$emotion,-1)))

    for(k in 1:(length(emotion_marker)-1)){
      emotion_start <- emotion_marker[k]
      emotion_end <- emotion_marker[k+1]-1
      emotion_state <- song_section$emotion[emotion_start]
      emotion_mean_gsr <- mean(song_section$gsr[emotion_start:emotion_end])
      emotion_mean_hr <- mean(song_section$hr[emotion_start:emotion_end])
      
      if (emotion_state==1){
        neutre_gsr_vector <- c(neutre_gsr_vector, emotion_mean_gsr)
        neutre_hr_vector <- c(neutre_hr_vector, emotion_mean_hr)
      }
      else if (emotion_state==2){
        faible_gsr_vector <- c(faible_gsr_vector, emotion_mean_gsr)
        faible_hr_vector <- c(faible_hr_vector, emotion_mean_hr)
      }
      else if (emotion_state==3){
        intense_gsr_vector <- c(intense_gsr_vector, emotion_mean_gsr)
        intense_hr_vector <- c(intense_hr_vector, emotion_mean_hr)
      }
      else if (emotion_state==4){
        frisson_gsr_vector <- c(frisson_gsr_vector, emotion_mean_gsr)
        frisson_hr_vector <- c(frisson_hr_vector, emotion_mean_hr)
      }
      else if (emotion_state==0){
        
      }
      
    } #end loop emotion
    
  } #end loop song
  
  gsr_row <- matrix(, nrow=1,ncol=5)
  gsr_row[1,1] <- mean(pause_gsr)
  gsr_row[1,2] <- mean(neutre_gsr_vector)
  gsr_row[1,3] <- mean(faible_gsr_vector)
  gsr_row[1,4] <- mean(intense_gsr_vector)
  gsr_row[1,5] <- mean(frisson_gsr_vector)
  
  hr_row <- matrix(, nrow=1,ncol=5)
  hr_row[1,1] <- mean(pause_hr)
  hr_row[1,2] <- mean(neutre_hr_vector)
  hr_row[1,3] <- mean(faible_hr_vector)
  hr_row[1,4] <- mean(intense_hr_vector)
  hr_row[1,5] <- mean(frisson_hr_vector)
  
  return(list(gsr=gsr_row,hr=hr_row))
}

wb <- createWorkbook("WB")

folder <- choose.dir(caption = "Select folder")
files <- list.files(folder,pattern = "\\.txt$",full.names = TRUE)

first_col <- matrix(c('Subject'))
gsr_result <- t(matrix(c('Pause','Neutre','Faible','Intense','Frisson')))
hr_result <- t(matrix(c('Pause','Neutre','Faible','Intense','Frisson')))
for (nfile in 1:length(files)){
  filename <- files[nfile]
  read_data <- read_table2(filename,col_names = c('time','neutre','faible','intense','frisson','song','gsr','hr','hrv'))
  result <- compute_mean_emotiv(read_data)
  gsr_result <- rbind(gsr_result,result$gsr)
  hr_result <- rbind(hr_result,result$hr)
  
  file_fullname <- unlist(strsplit(filename,'/'))[2]
  subject_name <- unlist(strsplit(file_fullname,'_'))[1]
  first_col <- rbind(first_col,subject_name)
}

gsr_towrite <- cbind(first_col,gsr_result) 
hr_towrite <- cbind(first_col,hr_result) 

addWorksheet(wb, "GSR")
writeData(wb, sheet = 1, gsr_towrite, colNames = FALSE, rowNames=FALSE)
addWorksheet(wb, "HR")
writeData(wb, sheet = 2, hr_towrite, colNames = FALSE, rowNames=FALSE)

saveWorkbook(wb, "Emotiv_GSR-HR_result.xlsx", overwrite = FALSE)

print('Finished Operation for Emotiv')
