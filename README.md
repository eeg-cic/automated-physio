# Automated Physio

This program compute mean of GSR and HR for each emotion.  
The program use the input file which is exported from the PPG Graph program.  
All the file should be in the same folder and each file shouldn't have header.  

- [+The program has seperate main.R for each device so make sure that the input folder is separated into each device as well+]  

For example
  1. Create a folder containing all the synchronized emotive .txt file
  2. Run main_emotiv
  3. Choose that folder

## Cautions

The subject name is taken from the file name.  
So make sure to keep to the same format of `subjectname_xxxxx_xxxxx.txt`
The important thing is the `_` after each subject name.  
The program will read the first word before the `_` and determine that it is the subject name.  

Example: [+AA13EmotivS1_Physio_synchronized.txt+]

- [-When using the program please make sure that the old result file is moved to a different folder or changed to a different name.-]    
- [-Because the program will give an error at the end if there is already the result file in the folder.-]
